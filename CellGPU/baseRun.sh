#$ -S /bin/sh
#$ -cwd
#$ -j y
#$ -l virtual_free=1G -l h_vmem=1G

cp $SGE_O_WORKDIR/* $TMPDIR
cd $TMPDIR
(time ./voronoi.out -n "{number}" -t "{tSteps}" -i "{iniSteps}" -e "{deltaT}" -p "{perimeter}" -a "{area}" -v "{velocity}" -s "{SubInt}" -z "{Tau}" -r "{IncVal}" -w "{dx}" -l "{length}") &> time.txt
cp * $SGE_O_WORKDIR/
rm *
