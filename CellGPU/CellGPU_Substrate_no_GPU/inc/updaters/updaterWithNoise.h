#ifndef updaterWithNoise_H
#define updaterWithNoise_H

#include "updater.h"
#include "noiseSource.h"



/*! \file updaterWithNoise.h */
//! an updater with a noise source

class updaterWithNoise : public updater
    {
    public:
        //!updaterWithNoise constructor
        updaterWithNoise(){};
        //!updaterWithNoise constructor
        updaterWithNoise(bool rep)
            {
            setReproducible(rep);
            };
        //!Set whether the source of noise should always use the same random numbers
        virtual void setReproducible(bool rep)
            {
            noise.setReproducible(rep);
            };
        //!re-index the any RNGs associated with the e.o.m.
 
    protected:
        //! A source of noise for the equation of motion
        noiseSource noise;
    };



#endif
