
#include "setTotalLinearMomentum.h"
/*! \file setTotalLinearMomentum.cpp */

setTotalLinearMomentum::setTotalLinearMomentum(Dscalar px, Dscalar py)
    {
    setMomentumTarget(px,py);
    };

void setTotalLinearMomentum::setMomentumTarget(Dscalar px, Dscalar py)
    {
    pHelper.resize(2);
    ArrayHandle<Dscalar2> h_ph(pHelper);
    h_ph.data[0] = make_Dscalar2(px,py);
    h_ph.data[1] = make_Dscalar2(0,0);
    //the following will be auto-resized once performUpdate is called
    pArray.resize(1);
    pIntermediateReduction.resize(1);
    };

void setTotalLinearMomentum::performUpdate()
    {
    int N = model->getNumberOfDegreesOfFreedom();
    if(N!=pArray.getNumElements())
        {
        pArray.resize(N);
        pIntermediateReduction.resize(N);
        };
    if(GPUcompute)
        setLinearMomentumCPU();
    else
        setLinearMomentumCPU();
    };

/*!
v[i] = v[i] + (1/(N*m[i]))*(Ptarget - Pcurrent)
*/
void setTotalLinearMomentum::setLinearMomentumCPU()
    {
    ArrayHandle<Dscalar2> h_ph(pHelper);
    ArrayHandle<Dscalar2> h_v(model->returnVelocities());
    ArrayHandle<Dscalar>  h_m(model->returnMasses());
    int N = model->getNumberOfDegreesOfFreedom();
    //!update the current P
    h_ph.data[1]  = make_Dscalar2(0.0,0.0);
    for (int ii = 0; ii < N; ++ii)
        h_ph.data[1] = h_ph.data[1] + h_m.data[ii]*h_v.data[ii];
    Dscalar2 Pshift = make_Dscalar2(h_ph.data[0].x-h_ph.data[1].x,h_ph.data[0].y-h_ph.data[1].y);
    for (int ii = 0; ii < N; ++ii)
        {
        h_v.data[ii] = h_v.data[ii]+(1.0/(N*h_m.data[ii]))*Pshift;
        };
    };

/*!
v[i] = v[i] + (1/(N*m[i]))*(Ptarget - Pcurrent)
*/
