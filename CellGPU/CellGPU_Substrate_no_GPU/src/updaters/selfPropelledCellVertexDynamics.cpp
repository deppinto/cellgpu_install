
#include "selfPropelledCellVertexDynamics.h"
/*! \file selfPropelledCellVertexDynamics.cpp */

/*!
An extremely simple constructor that does nothing, but enforces default GPU operation
\param the number of points in the system (cells or particles)
*/
selfPropelledCellVertexDynamics::selfPropelledCellVertexDynamics(int _Ncells, int _Nvertices)
    {
    Timestep = 0;
    deltaT = 0.01;
    GPUcompute = false;
    mu = 1.0;
    Ndof = _Nvertices;
    Nvertices = _Nvertices;
    Ncells = _Ncells;
    noise.initialize(Ncells);
    displacements.resize(Nvertices);
    };

/*!
Advances self-propelled dynamics with random noise in the director by one time step
*/
void selfPropelledCellVertexDynamics::integrateEquationsOfMotion()
    {
    Timestep += 1;
    if (activeModel->getNumberOfDegreesOfFreedom() != Nvertices)
        {
        Nvertices = activeModel->getNumberOfDegreesOfFreedom();
        displacements.resize(Nvertices);
        Ncells = Nvertices / 2;
        noise.initialize(Ncells);
        };
    if(GPUcompute)
        {
        integrateEquationsOfMotionCPU();
        }
    else
        {
        integrateEquationsOfMotionCPU();
        }
    }

/*!
The straightforward GPU implementation
*/

/*!
Move every vertex according to the net force on it and its motility...CPU routine
*/
void selfPropelledCellVertexDynamics::integrateEquationsOfMotionCPU()
    {
    activeModel->computeForces();
    {//scope for array Handles
    ArrayHandle<Dscalar2> h_f(activeModel->returnForces(),access_location::host,access_mode::read);
    ArrayHandle<Dscalar> h_cd(activeModel->cellDirectors,access_location::host,access_mode::readwrite);
    ArrayHandle<Dscalar2> h_disp(displacements,access_location::host,access_mode::overwrite);
    ArrayHandle<Dscalar2> h_motility(activeModel->Motility,access_location::host,access_mode::read);
    ArrayHandle<int> h_vcn(activeModel->vertexCellNeighbors,access_location::host,access_mode::read);

    Dscalar directorx,directory;
    Dscalar2 disp;
    for (int i = 0; i < Nvertices; ++i)
        {
        Dscalar v1 = h_motility.data[h_vcn.data[3*i]].x;
        Dscalar v2 = h_motility.data[h_vcn.data[3*i+1]].x;
        Dscalar v3 = h_motility.data[h_vcn.data[3*i+2]].x;
        //for uniform v0, the vertex director is the straight average of the directors of the cell neighbors
        directorx  = v1*cos(h_cd.data[ h_vcn.data[3*i] ]);
        directorx += v2*cos(h_cd.data[ h_vcn.data[3*i+1] ]);
        directorx += v3*cos(h_cd.data[ h_vcn.data[3*i+2] ]);
        directorx /= 3.0;
        directory  = v1*sin(h_cd.data[ h_vcn.data[3*i] ]);
        directory += v2*sin(h_cd.data[ h_vcn.data[3*i+1] ]);
        directory += v3*sin(h_cd.data[ h_vcn.data[3*i+2] ]);
        directory /= 3.0;
        //move vertices
        h_disp.data[i].x = deltaT*(directorx + mu*h_f.data[i].x);
        h_disp.data[i].y = deltaT*(directory + mu*h_f.data[i].y);
        };

    //update cell directors
    for (int i = 0; i < Ncells; ++i)
        {
        Dscalar randomNumber = noise.getRealNormal();
        Dscalar Dr = h_motility.data[i].y;
        h_cd.data[i] += randomNumber*sqrt(2.0*deltaT*Dr);
        };
    }//end array handle scoping
    activeModel->moveDegreesOfFreedom(displacements);
    activeModel->enforceTopology();
    };
