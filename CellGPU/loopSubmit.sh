# !/bin/bash   

cd /home/diogo/CellGPU/CellGPU_Substrate_no_GPU/
make
cd /home/diogo/CellGPU/

SCRIPT=1

mkdir -p scripts"$SCRIPT"
mkdir -p submit"$SCRIPT"

START=1
END=2
SEED_VAR=1

for ((ii=$START; ii<=$END; ii++))
do

JOBS=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $14}'`
#echo "$JOBS" 
for ((CURRJOB=$START; CURRJOB<=JOBS; CURRJOB++))
do

#echo "$SEED_VAR"
n=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $1}'`
t=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $2}'`
g=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $3}'`
i=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $4}'`
e=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $5}'`
p=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $6}'`
a=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $7}'`
v=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $8}'`
s=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $9}'`
z=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $10}'`
r=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $11}'`
w=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $12}'`
l=`cat dados.txt | sed -n ${ii}','${ii}'p' | awk '{print $13}'`

cd scripts"$SCRIPT"/
mkdir -p Job_"$SEED_VAR"/
cd ..
cd submit"$SCRIPT"/
mkdir -p Job_"$SEED_VAR"/
cd ..
#echo "$n $t $g $i $e $p $a $v $s $z $r $w"

cp -r /home/diogo/CellGPU/CellGPU_Substrate2/voronoi.out /home/diogo/CellGPU/scripts"$SCRIPT"/Job_"$SEED_VAR"

cp baseRun.sh ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{number}/$n/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{perimeter}/$p/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{iniSteps}/$i/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{tSteps}/$t/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{deltaT}/$e/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{area}/$a/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{velocity}/$v/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{SubInt}/$s/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{Tau}/$z/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{IncVal}/$r/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{dx}/$w/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{length}/$l/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
sed -i "s/{Job}/$SEED_VAR/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh

cp baseSubmit.submit ./scripts"$SCRIPT"/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
sed -i "s/{SubInt}/$s/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
sed -i "s/{Tau}/$z/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
sed -i "s/{IncVal}/$r/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
sed -i "s/{perimeter}/$p/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
sed -i "s/{area}/$a/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
sed -i "s/{velocity}/$v/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
sed -i "s/{Job}/$SEED_VAR/g" ./scripts"$SCRIPT"/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit

#sleep 1s

if [ $(find ./scripts"$SCRIPT"/Job_"$SEED_VAR"/ -name "*nc") ]; then 
  echo "File is found in Job_"$SEED_VAR""
else
  echo "File not found in Job_"$SEED_VAR""
  #condor_submit ./scripts1/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
  cd scripts"$SCRIPT"
  cd Job_"$SEED_VAR"
  qsub -V -l qm=true run_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.sh
  cd /home/diogo/CellGPU/
fi

#condor_submit ./scripts/Job_"$SEED_VAR"/Submit_St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit
#echo ./scripts/Job_"$SEED_VAR"/Submit__St${s}_T${z}_Iv${r}_P${p}_A${a}_V${v}.submit




((SEED_VAR++))

done
done
