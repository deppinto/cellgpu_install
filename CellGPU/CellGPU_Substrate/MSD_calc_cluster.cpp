#include "std_include.h"

#include "cuda_runtime.h"
#include "cuda_profiler_api.h"

#define ENABLE_CUDA

#include "Simulation.h"

#include "voronoiQuadraticEnergy.h"
#include "selfPropelledParticleDynamics.h"
#include "DatabaseNetCDFSPV.h"

#include <cstring>

using namespace std;

int main(){

    int cont=0;
    for(int ini=1; ini<=1; ini++){
 
        //int ini_time=0;//(b-b/ini);
        int N=256;
        int a=N;
        int b=2081;
        int total_size=10000000;
        vector <double> MSD (total_size, 0);
        vector <double> Q (total_size, 0);
        double tau_samples=0;
        vector <double> t0 (total_size, 0);
        int size_MSD=b*N;
        vector <double> MSD_i (size_MSD,0);
        int amostras=100;
        int start=1;
        int fim=8;
        double tau_a=-1.0;
        vector <double> time(b,0);
        double min_p0=0;
        double max_p0=10;
        int time_delta=0;
        string filename;
        
        //shared_ptr<VoronoiQuadraticEnergy> spv  = make_shared<VoronoiQuadraticEnergy>(N, false);
        shared_ptr<VoronoiQuadraticEnergy> spv  = make_shared<VoronoiQuadraticEnergy>(N,1.0,4.0,true);
        FILE *f;
 
        for(int avg=start; avg<=fim*amostras; avg++){
            cont=cont+1;
            vector <double> unrappedposx(N,0);
            vector <double> unrappedposy(N,0);
            
            vector <vector <double> > vardata(2*N, vector<double> (b, 0));
            //double vardata[2*N][b];
            double L;
            vector <vector <double> > vararea(2*N, vector<double> (b, 0));
            //double vararea[2*N][b];
            vector <vector <double> > varper(N, vector<double> (b, 0));
            //double varper[N][b];
            //double val1=0;
            //double val2=0;
            //double val3=0;
            //double val4=0;

            int curr_line=(avg-1)/amostras+1;           
            int v1, v2, v3, v4, v9, v12, v14;
            double v5, v6, v7, v8, v10, v11, v13;
            string access_dados="/home/destevao/dados.txt";
            ifstream file(access_dados);

            for(int line=0; line<curr_line; line++){
                file>> v1 >> v2 >> v3 >> v4 >> v5 >> v6 >> v7 >> v8 >> v9 >> v10 >> v11 >> v12 >> v13 >> v14;
            }

            file.close();

            string add_JOB_num=to_string(avg);
            //if(avg<10)add_JOB_num="00"+to_string(avg);
            //else if(avg<100)add_JOB_num="0"+to_string(avg);
            //else add_JOB_num=to_string(avg);
            
            cout<<add_JOB_num<<endl;
           
            char dataname[1000000]; 
            sprintf(dataname, "/home/destevao/scripts/Job_%d/test_voronoi_St%d_T%g_Iv%g_P%g_A%g_V%g.nc", avg, v9, v10, v11, v6, v7, v8);
            //printf("%s\n",dataname);
            SPVDatabaseNetCDF ncdat(N,dataname,NcFile::ReadOnly);
            //Check if the file exists in the output folder. if it does then do the scan
            if ((f = fopen(dataname, "r")) == NULL){
                printf("Error! opening file\n");
                printf("%s\n",dataname);
                exit (911);
                return -1;
            }
            else{
                fclose(f);
                ncdat.ReadState(spv,0,false);
                b=ncdat.GetNumRecs();
            //    printf("b=%u\n", b);
            }
           
            ArrayHandle<Dscalar2> h_p(spv->cellPositions,access_location::host,access_mode::read);
            ArrayHandle<Dscalar2> h_ap(spv->AreaPeriPreferences,access_location::host,access_mode::read);
            ArrayHandle<Dscalar2> h_AP(spv->AreaPeri,access_location::host,access_mode::read);

            Dscalar x11,x12,x21,x22;
            spv->Box->getBoxDims(x11,x12,x21,x22);
            L=x11;

            for(int r=0; r<b; r++){
                if(r>0)ncdat.ReadState(spv,r,false);
                time[r]= spv->currentTime; 
                int rrr=0;
                for (int rr = 0; rr < N; ++rr){
                
                    vardata[rrr][r]=h_p.data[rr].x;
                    vardata[rrr+1][r]=h_p.data[rr].y;
                
                    vararea[rr][r]=h_ap.data[rr].x;
                    vararea[N+rr][r]=h_AP.data[rr].x;
                    
                    varper[rr][r]=h_ap.data[rr].y;
                    
                    rrr=rrr+2;
                }
            }
           
            /*
            filename="/Users/dpinto/Desktop/scripts_grf_l/Job_"+add_JOB_num+"/pos.txt";
            ifstream file(filename);
            
            for(int r=0; r<b; r++){
                for(int rr=0; rr<2*N; rr++){
                    file>> val1;
                    vardata[rr][r]=val1;
                }
            }
            file.close();
 
            
            filename="/Users/dpinto/Desktop/scripts_grf_l/Job_"+add_JOB_num+"/Area.txt";
            file.open(filename);
            
            for(int r=0; r<b; r++){
                for(int rr=0; rr<2*N; rr++){
                    file>> val2;
                    vararea[rr][r]=val2;
                }
            }
            file.close();
            
            filename="/Users/dpinto/Desktop/scripts_grf_l/Job_"+add_JOB_num+"/Perimeter.txt";
            file.open(filename);
            
            for(int r=0; r<b; r++){
                for(int rr=0; rr<N; rr++){
                    file>> val4;
                    varper[rr][r]=5;
                }
            }
            file.close();
            
            filename="/Users/dpinto/Desktop/scripts_grf_l/Job_"+add_JOB_num+"/box.txt";
            file.open(filename);
            
            file>> L;
            file.close();
            
            filename="/Users/dpinto/Desktop/scripts_grf_l/Job_"+add_JOB_num+"/time.txt";
            file.open(filename);
            
            for(int rr=0; rr<b; rr++){
                file>> val3;
                time[rr]=val3;
            }
            file.close();*/
            
            //vardata=ncread(listings(lt).name, 'pos');
            //Box=ncread(listings(lt).name, 'BoxMatrix');
            //time=ncread(listings(lt).name, 'time');
  
            int vec[6]={1350, 1500, 1650, 1700, 1850, 2000};
            //int ini_time=(b-b/ini);
            int ini_time=vec[ini-1];

            if((cont-1)%amostras==0 || cont==1){
                fill(MSD.begin(), MSD.end(), 0);
                fill(Q.begin(), Q.end(), 0);
                fill(t0.begin(), t0.end(), 0);
                fill(MSD_i.begin(), MSD_i.end(), 0);
                t0[0]=1.0;
                tau_a=-1;
            }

            //Qtest=zeros([b-ini_time 1]);
            int tau_bool=-1;
            vector <double> xlast(N, 0);
            vector <double> ylast(N, 0);
            for(int nn = 0; nn<b; nn++){
                
                vector <double> xn(N, 0);
                vector <double> yn(N, 0);
                double xm = 0.0;
                double ym = 0.0;
                int site=0;
                for(int cc=0; cc<2*N; cc=cc+2){
                    xn[site] = vardata[cc][nn];
                    yn[site] = vardata[cc+1][nn];
                    
                    xlast[site]=xn[site];
                    ylast[site]=yn[site];
                    
                    unrappedposy[site]=yn[site];
                    unrappedposx[site]=xn[site];
                    
                    xm=xm+xn[site];
                    ym=ym+yn[site];
                    
                    site++;
                }
                xm=xm/N;
                ym=ym/N;
    
                /*double xm = 0.0;
                double ym = 0.0;
                double max_dim=N;
        
                for(int ii=0; ii<max_dim; ii++){
                    xm=xm+xn[ii];
                    ym=ym+yn[ii];
                }
                xm=xm/max_dim;
                ym=ym/max_dim;*/
    
                for(int n = nn+1; n<b; n++){
                    
                    time_delta=(time[n]-time[nn])/0.001;
                    
                    vector <double> x(N, 0);
                    vector <double> y(N, 0);
                    double xmi=0;
                    double ymi=0;
                    site=0;
                    for(int cc=0; cc<2*N; cc=cc+2){
                        x[site] = vardata[cc][n];
                        y[site] = vardata[cc+1][n];
                        
                        xmi=xmi+x[site];
                        ymi=ymi+y[site];
                        
                        site++;
                    }
                    xmi=xmi/N;
                    ymi=ymi/N;
                    
                    
                    /*double xmi=0;
                    double ymi=0;
            
                    for (int ii=0; ii<N; ii++){
                        xmi=xmi+x[ii];
                        ymi=ymi+y[ii];
                    }
                    xmi=xmi/N;
                    ymi=ymi/N;*/
                    
                    double posxm=abs(xmi-xm);
                    double posym=abs(ymi-ym);
    
                    if(posxm>L/2){
                        posxm=L-posxm;
                    }
                    if(posym>L/2){
                        posym=L-posym;
                    }
    
                    double maxVal=-100000;
                    int diff_N=0;
                    
                    
                    for(int ii=0; ii<N; ii++){
                        if(varper[ii][n]>min_p0 && varper[ii][n]<max_p0){
                            double posx=x[ii]-xlast[ii];
                            double posy=y[ii]-ylast[ii];
                            if(posx>L/2)posx=L-posx;
                            if(posx<-L/2)posx=-L-posx;
                            if(posy>L/2)posy=L-posy;
                            if(posy<-L/2)posy=-L-posy;
                            unrappedposx[ii]=unrappedposx[ii]+posx;
                            unrappedposy[ii]=unrappedposy[ii]+posy;
                            posx=unrappedposx[ii]-xn[ii];
                            posy=unrappedposy[ii]-yn[ii];
                            posx=posx-posxm;
                            posy=posy-posym;
                            double final_calc=posx*posx+posy*posy;
                            diff_N++;
                            
                            int idx=ii+(n-nn)*N;
                            MSD_i[idx]=MSD_i[idx]+final_calc;
                            MSD[time_delta]=MSD[time_delta]+final_calc/N;
                            if(maxVal<final_calc/N)maxVal=final_calc;
                            if(sqrt(final_calc)<sqrt(vararea[ii][n])/2)Q[time_delta]=Q[time_delta]+1;
                            xlast[ii]=x[ii];
                            ylast[ii]=y[ii];
                        }
                    }
                    t0[time_delta]=t0[time_delta]+1;
                    //if(diff_N>0){
                      //  MSD[time_delta]=MSD[time_delta]/diff_N;
                      //  Q[time_delta]=Q[time_delta]/diff_N;
                    //}
                }
            }
            
            if(cont%amostras==0 && cont>0){
                int value=cont/amostras;
                filename="/home/destevao/CellGPUSubstrate/Results/Avg/data_grf_"+to_string(value)+".txt";
                ofstream file1;
                file1.open (filename);
                for(int n = 0; n<total_size; n++){

                    if(MSD[n]>0){
                        t0[n]=t0[n];
                        Q[n]=Q[n]/N;
                        //time[n]=time[n]-(10000*0.001);
            
                        MSD[n]=MSD[n]/t0[n];
                        Q[n]=Q[n]/t0[n];
                    
                        if(Q[n]<exp(-1) && tau_a<0 && n>0)tau_a=n*0.001;
                        
                        file1 << MSD[n] << " " << Q[n] << " " << n*0.001 << " " << tau_a << " "<< t0[n] <<endl;
                    }
                }
                file1.close();

                filename="/home/destevao/CellGPUSubstrate/Results/"+to_string(ini)+"/data_distribution_"+to_string(value)+".txt";
                //ofstream file1;
                file1.open (filename);
                for(int n = 0; n<b*N; n++){
                    int y=n/N;
                    int x=n%N;
                    //if(MSD_i[n]){
                
		        file1 << MSD_i[n]/amostras << " " << time[y]/0.001 << " " << x << " " << y << endl;

                    //}
                }
                file1.close();
            }
        }
        
        
    }
    return 0;
}
