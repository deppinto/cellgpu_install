#include "std_include.h"

#include "cuda_runtime.h"
#include "cuda_profiler_api.h"

#define ENABLE_CUDA

#include "Simulation.h"

#include "voronoiQuadraticEnergy.h"
#include "selfPropelledParticleDynamics.h"
#include "DatabaseNetCDFSPV.h"

int main(){

    int numpts = 400; //number of cells
    shared_ptr<VoronoiQuadraticEnergy> spv  = make_shared<VoronoiQuadraticEnergy>(numpts,1.0,4.0,true);
    FILE *f;
    int rec;
    double dt=0.01;

    char dataname[1000000];
    sprintf(dataname,"/home/destevao/test_voronoi.nc");
    printf("%s\n",dataname);
    SPVDatabaseNetCDF ncdat(numpts,dataname,NcFile::ReadOnly);
    //Check if the file exists in the output folder. if it does then do the scan
    if ((f = fopen(dataname, "r")) == NULL){
        printf("Error! opening file\n");
        printf("%s\n",dataname);
        return -1;
    }
    else{
        fclose(f);
        ncdat.ReadState(spv,0,false);
        rec=ncdat.GetNumRecs();
        printf("rec=%u\n",rec);
    }

    ArrayHandle<Dscalar2> h_p(spv->cellPositions,access_location::host,access_mode::read);
    vector<Dscalar2> pos0(numpts, make_Dscalar2(0.0, 0.0));

    for (int ii = 0; ii < numpts; ++ii){
        pos0[ii].x=h_p.data[ii].x;
        pos0[ii].y=h_p.data[ii].y;
    }


    vector<Dscalar> MSD(rec, 0.0);
    Dscalar posDiffx;
    Dscalar posDiffy;
    Dscalar x11,x12,x21,x22;
    spv->Box->getBoxDims(x11,x12,x21,x22);
    Dscalar t;
    cout<<"Box Size: "<<x11<<" "<<x22<<endl;
    for (int ii = 1; ii<rec; ++ii){
        ncdat.ReadState(spv,ii,false);
        ArrayHandle<Dscalar2> h_p(spv->cellPositions,access_location::host,access_mode::read);
        t=spv->currentTime;

        for (int jj = 0; jj < numpts; ++jj){
            posDiffx=abs(h_p.data[jj].x-pos0[jj].x);
            posDiffy=abs(h_p.data[jj].y-pos0[jj].y);
            if(posDiffx>x11/2)posDiffx=x11-posDiffx;
            if(posDiffy>x22/2)posDiffy=x22-posDiffy;
            MSD[ii]+=posDiffx*posDiffx+posDiffy*posDiffy;
        }
        MSD[ii]=MSD[ii]/numpts;
        cout<<t/dt <<" "<<MSD[ii]<<endl;
    }

    //Gonca method

    int num=0;
    double x0,y0,xcm0,ycm0,xcm,ycm,xcurr,ycurr,xdiff,ydiff,norm,total,msd;

    vector<Dscalar> MSDTotalList(1000000);
    for(int i=0; i<rec; i++){
        MSDTotalList[i]=0;
    }
    ncdat.ReadState(spv,0,false);
    rec=ncdat.GetNumRecs();

    vector<Dscalar> MSDlist(rec);
    ArrayHandle<Dscalar2> h_cpos(spv->cellPositions);
    vector<Dscalar> cposdatlast(2*numpts);
    vector<Dscalar> cposdatnext(2*numpts);
    Dscalar2 nlastp, nnextp, rij;
    vector<Dscalar> deltalist(2*numpts);
    vector<Dscalar> unwrappedpos(rec*2*numpts);

    int currtime=0;
    ncdat.ReadState(spv,currtime,false);
    for (int cellid = 0; cellid < numpts; ++cellid){
        unwrappedpos[(currtime*2*numpts) + (2*cellid)] = h_cpos.data[cellid].x;
        unwrappedpos[(currtime*2*numpts) + (2*cellid) + 1] = h_cpos.data[cellid].y;    
    }



    for (int jj=0; jj<rec-1; ++jj){
        ncdat.ReadState(spv,jj,false);
        for (int ii = 0; ii < numpts; ++ii){
            Dscalar px = h_cpos.data[ii].x;
            Dscalar py = h_cpos.data[ii].y;
            cposdatlast[(2*ii)] = px;
            cposdatlast[(2*ii)+1] = py;
        };
        ncdat.ReadState(spv,jj+1,false);
        for (int ii = 0; ii < numpts; ++ii)
        {
            Dscalar px = h_cpos.data[ii].x;
            Dscalar py = h_cpos.data[ii].y;
            cposdatnext[(2*ii)] = px;
            cposdatnext[(2*ii)+1] = py;
        };

        for (int ii = 0; ii < numpts; ++ii)
        {
            nlastp.x = cposdatlast[(2*ii)];
            nlastp.y = cposdatlast[(2*ii)+1];
            nnextp.x = cposdatnext[(2*ii)];
            nnextp.y = cposdatnext[(2*ii)+1];
            spv->Box->minDist(nlastp,nnextp,rij);
            deltalist[(2*ii)] = rij.x;
            deltalist[((2*ii)+1)] = rij.y;
        };
                
        for (int ii = 0; ii < numpts; ++ii)
        {
            unwrappedpos[((jj+1)*2*numpts) + (2*ii)] = unwrappedpos[((jj)*2*numpts) + (2*ii)]+deltalist[(2*ii)];
            unwrappedpos[((jj+1)*2*numpts) + (2*ii) + 1] = unwrappedpos[((jj)*2*numpts) + (2*ii)+1]+deltalist[(2*ii)+1];
        };
    }


    xcm0=0;
    ycm0=0;
    for (int cellid = 0; cellid < numpts; ++cellid){
        xcm0+=unwrappedpos[(0*2*numpts) + (2*cellid)] ;
        ycm0+=unwrappedpos[(0*2*numpts) + (2*cellid) + 1];    
    }
    xcm0=xcm0/numpts;
    ycm0=ycm0/numpts;
    MSDlist[0]=0;
    //printf("MSDlist[0]=%.9lf\n", MSDlist[0]);
    MSDTotalList[0]+=MSDlist[0];  


    for (int jj=1; jj<rec; ++jj){
        xcm=0;
        ycm=0;
        for (int cellid = 0; cellid < numpts; ++cellid){                                                 xcm+=unwrappedpos[(jj*2*numpts) + (2*cellid)] ;
            ycm+=unwrappedpos[(jj*2*numpts) + (2*cellid) + 1];
        }
        xcm=xcm/numpts;
        ycm=ycm/numpts;

        total=0;
        for (int cellid = 0; cellid < numpts; ++cellid){
            xdiff=(unwrappedpos[(jj*2*numpts) + (2*cellid)]-unwrappedpos[(0*2*numpts) + (2*cellid)]-(xcm-xcm0));
            ydiff=(unwrappedpos[(jj*2*numpts) + (2*cellid) + 1]-unwrappedpos[(0*2*numpts) + (2*cellid) + 1]-(ycm-ycm0));                                                                                                
            norm=sqrt( (xdiff*xdiff) + (ydiff*ydiff) );
            total+=norm*norm;
        }
        MSDlist[jj]=total/numpts;
        //cout<<MSDlist[jj]<<endl; 
        //printf("MSDlist[%i]=%.9lf\n", jj, MSDlist[jj]);
	MSDTotalList[jj]+=MSDlist[jj];
    }



return 0;
}
